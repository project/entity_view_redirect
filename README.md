# Entity View Redirect

Entity View Redirect is a simple module that helps to add redirects to the Entities like Node, Taxonomy, and User view page to Edit or custom internal URLs. The advantage of this module is able to skip the View pages for the Drupal core entities. The websites used for Storing content rather than for displaying purposes can use this module.

In some cases, we might use Views to build the pages. In those scenarios, we can use this module to define the path need to redirect.

For a full description of the module, visit the
[Entity View Redirect](https://www.drupal.org/project/entity_view_redirect).

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

## Maintainers

- Arunkumar Kuppuswamy [arunkumark](https://www.drupal.org/u/arunkumark).