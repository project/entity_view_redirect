<?php

namespace Drupal\entity_view_redirect\EventSubscriber;

use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Node edit redirect event subscriber.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;
  
  /**
   * Constructs a new class object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, AccountInterface $current_user) {
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
  }

  /**
   * Event subscriber callback.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The Event to process.
   */
  public function redirectToEditPage(RequestEvent $event) {
    // Check if the user has the 'bypass redirect' permission.
    if ($this->currentUser->hasPermission('bypass entity view redirect')) {
      return;
    }

    if ($this->routeMatch->getRouteName() == 'entity.node.canonical') {
      $node = $this->routeMatch->getParameter('node');
      if ($node instanceof NodeInterface) {
        $node_type = $node->type->entity;
        if ($node_type->getThirdPartySetting('entity_view_redirect', 'node_view_redirect', FALSE)) {
          if ($custom_path = $node_type->getThirdPartySetting('entity_view_redirect', 'node_redirect_path', FALSE)) {
            if (str_contains($custom_path, '%')) {
              $custom_path = str_replace('%', $node->id(), $custom_path);
            }
            $url = Url::fromUri('internal:' . $custom_path)->setAbsolute()->toString();
          }
          else {
            $url = Url::fromRoute('entity.node.edit_form', ['node' => $node->id()])->setAbsolute()->toString();
          }
          $event->setResponse(new RedirectResponse($url));
        }
      }
    }
    elseif ($this->routeMatch->getRouteName() == 'entity.taxonomy_term.canonical') {
      $term = $this->routeMatch->getParameter('taxonomy_term');
      if ($term instanceof TermInterface) {
        $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($term->bundle());

        if ($vocabulary->getThirdPartySetting('entity_view_redirect', 'vocabulary_view_redirect', FALSE)) {
          if ($custom_path = $vocabulary->getThirdPartySetting('entity_view_redirect', 'vocabulary_redirect_path', FALSE)) {
            if (str_contains($custom_path, '%')) {
              $custom_path = str_replace('%', $term->id(), $custom_path);
            }
            $url = Url::fromUri('internal:' . $custom_path)->setAbsolute()->toString();
          }
          else {
            $url = Url::fromRoute('entity.taxonomy_term.edit_form', ['taxonomy_term' => $term->id()])->setAbsolute()->toString();
          }
          $event->setResponse(new RedirectResponse($url));
        }
      }
    }
    elseif ($this->routeMatch->getRouteName() == 'entity.user.canonical') {
      $user = $this->routeMatch->getParameter('user');
      if ($user instanceof UserInterface) {
        $entity_view_settings = $this->configFactory->getEditable('entity_view_redirect.settings');

        if ($entity_view_settings->get('user_view_redirect', FALSE)) {
          if ($custom_path = $entity_view_settings->get('user_redirect_path', FALSE)) {
            if (str_contains($custom_path, '%')) {
              $custom_path = str_replace('%', $user->id(), $custom_path);
            }
            $url = Url::fromUri('internal:' . $custom_path)->setAbsolute()->toString();
          }
          else {
            $url = Url::fromRoute('entity.user.edit_form', ['user' => $user->id()])->setAbsolute()->toString();
          }

          $event->setResponse(new RedirectResponse($url));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirectToEditPage'];
    return $events;
  }

}
